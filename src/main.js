import Vue from 'vue'
import App from './App.vue'
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Vue.config.productionTip = false

Sentry.init({
  Vue,
  dsn: "https://5e304622e0664bd0ad6d51731de89900@o488098.ingest.sentry.io/5547829",
  integrations: [
    new Integrations.BrowserTracing(),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

new Vue({
  render: h => h(App),
}).$mount('#app')